# Iranium Project

Iranium is a WordPress plugin for integrating Persian calendar system in to the core with the minimum impact. Iranium focuses on performance and stability, rather than developing more bugs or take sharp edges in the path of development.

This here is the documentation for the plugin API, available in English and Persian.

## License

All the contents are licensed under the [GNU General Public License v3.0](LICENSE).